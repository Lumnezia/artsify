const drawResult = () => {
  let val = parseInt(document.getElementById("element_size").value);

  if (typeof val === "number" && val > 2) {
    let file_chooser = document.getElementById("fileChooser");
    let files = file_chooser.files;
    let image_url;
    if (files.length) {
      let file = files[0];
      image_url = window.URL.createObjectURL(file);
    } else {
      image_url = "../res/sample.jpg";
    }

    let new_image = new Image();
    new_image.src = image_url;

    context_source.clearRect(0, 0, canvas_width, canvas_height);
    new_image.addEventListener("load", () => {
      canvas_height = new_image.naturalHeight;
      canvas_width = new_image.naturalWidth;

      canvas_source.setAttribute("height", canvas_height);
      canvas_source.setAttribute("width", canvas_width);

      canvas_result.setAttribute("height", canvas_height);
      canvas_result.setAttribute("width", canvas_width);

      context_source.drawImage(new_image, 0, 0);
      generateDrawing(val);
    });
  } else {
    alert("Please enter a valid number larger than 2");
  }
};

const generateDrawing = element_size => {
  context_result.clearRect(0, 0, canvas_width, canvas_height);

  const total_rows = Math.floor(canvas_width / element_size);
  const total_cols = Math.floor(canvas_height / element_size);
  let x = 0;
  let y = 0;

  base_type = document.getElementById("type_select").value;

  for (let col = 0; col < total_cols; col++) {
    for (let row = 0; row < total_rows; row++) {
      let image_data = context_source.getImageData(x, y, element_size, element_size).data;
      let color_data = getAverageColor(image_data);

      if (base_type === "random") {
        let options = ["circle", "square", "triangle", "diamond"];
        let key = Math.floor(Math.random() * 4);
        type = options[key];
      } else {
        type = base_type;
      }

      if (type === "circle") {
        drawCircle(context_result, x, y, element_size, `rgb(${color_data.red}, ${color_data.green}, ${color_data.blue})`);
      } else if (type === "square") {
        drawSquare(context_result, x, y, element_size, `rgb(${color_data.red}, ${color_data.green}, ${color_data.blue})`);
      } else if (type === "triangle") {
        drawTriangle(context_result, x, y, element_size, `rgb(${color_data.red}, ${color_data.green}, ${color_data.blue})`);
      } else if (type === "diamond") {
        drawDiamond(context_result, x, y, element_size, `rgb(${color_data.red}, ${color_data.green}, ${color_data.blue})`);
      }

      x += element_size;
    }
    x = 0;
    y += element_size;
  }
};
